package wxpay

import (
	"errors"
	"strconv"
	"strings"
)

// 解析下载对账单的原始返回
func ParseDownloadBillResponse(rsp string) (info DownloadBillInfo, err error) {
	lines := strings.Split(rsp, "\n")
	if len(lines) < 4 {
		err = errors.New("对账单格式错误")
		return
	}
	// 解析支付订单部分
	for _, line := range lines[1 : len(lines)-3] {
		lineParts := dbSplit(line)
		order := DownloadBillOrderItem{}
		order.TransactionTime = lineParts[0]
		order.AppID = lineParts[1]
		order.MchID = lineParts[2]
		order.SubMchID = lineParts[3]
		order.DeviceID = lineParts[4]
		order.TransactionId = lineParts[5]
		order.OutTradeNo = lineParts[6]
		order.UserToken = lineParts[7]
		order.TradeType = lineParts[8]
		order.TradeStatus = lineParts[9]
		order.BankType = lineParts[10]
		order.FeeType = lineParts[11]
		order.SettlementTotalFee = dbConvert(lineParts[12], 2, 100)
		order.CouponFee = dbConvert(lineParts[13], 2, 100)
		order.RefundId = lineParts[14]
		order.OutRefundNo = lineParts[15]
		order.SettlementRefundFee = dbConvert(lineParts[16], 2, 100)
		order.CouponRefundFee = dbConvert(lineParts[17], 2, 100)
		order.RefundType = lineParts[18]
		order.RefundStatus = lineParts[19]
		order.Body = lineParts[20]
		order.Attach = lineParts[21]
		order.ServiceFee = dbConvert(lineParts[22], 2, 100)
		order.ServiceFeeRate = lineParts[23]
		order.TotalFee = dbConvert(lineParts[24], 2, 100)
		order.RefundFee = dbConvert(lineParts[25], 2, 100)
		order.ServiceFeeRateRemark = lineParts[26]
		info.Orders = append(info.Orders, order)
	}
	// 解析汇总信息部分
	totalParts := dbSplit(lines[len(lines)-2])
	info.Total.TotalCount = dbConvert(totalParts[0], 0, 1)
	info.Total.SettlementTotalFee = dbConvert(totalParts[1], 2, 100)
	info.Total.SettlementRefundFee = dbConvert(totalParts[2], 2, 100)
	info.Total.CouponRefundFee = dbConvert(totalParts[3], 2, 100)
	info.Total.ServiceFee = dbConvert(totalParts[4], 2, 100)
	info.Total.TotalFee = dbConvert(totalParts[5], 2, 100)
	info.Total.RefundFee = dbConvert(totalParts[6], 2, 100)
	return
}

// 对账单解析后的结构体
type DownloadBillInfo struct {
	Orders []DownloadBillOrderItem `json:"orders"` // 所有支付记录
	Total  DownloadBillTotalItem   `json:"total"`  // 汇总信息
}

// 对账单解析后的单条支付记录
type DownloadBillOrderItem struct {
	TransactionTime      string `json:"transaction_time"`        // 交易时间，2020-06-29 10:59:49
	AppID                string `json:"app_id"`                  // 公众账号ID
	MchID                string `json:"mch_id"`                  // 商户号
	SubMchID             string `json:"sub_mch_id"`              // 特约商户号
	DeviceID             string `json:"device_id"`               // 设备号
	TransactionId        string `json:"transaction_id"`          // 微信订单号
	OutTradeNo           string `json:"out_trade_no"`            // 商户订单号
	UserToken            string `json:"user_token"`              // 用户标识
	TradeType            string `json:"trade_type"`              // 交易类型
	TradeStatus          string `json:"trade_status"`            // 交易状态
	BankType             string `json:"bank_type"`               // 付款银行
	FeeType              string `json:"fee_type"`                // 货币种类
	SettlementTotalFee   int    `json:"settlement_total_fee"`    // 应结订单金额，0.00
	CouponFee            int    `json:"coupon_fee"`              // 代金券金额
	RefundId             string `json:"refund_id"`               // 微信退款单号
	OutRefundNo          string `json:"out_refund_no"`           // 商户退款单号
	SettlementRefundFee  int    `json:"settlement_refund_fee"`   // 退款金额
	CouponRefundFee      int    `json:"coupon_refund_fee"`       // 充值券退款金额
	RefundType           string `json:"refund_type"`             // 退款类型
	RefundStatus         string `json:"refund_status"`           // 退款状态
	Body                 string `json:"body"`                    // 商品名称
	Attach               string `json:"attach"`                  // 商户数据包
	ServiceFee           int    `json:"service_fee"`             // 手续费
	ServiceFeeRate       string `json:"service_fee_rate"`        // 费率
	TotalFee             int    `json:"total_fee"`               // 订单金额
	RefundFee            int    `json:"refund_fee"`              // 申请退款金额
	ServiceFeeRateRemark string `json:"service_fee_rate_remark"` // 费率备注
}

// 对账单解析后的汇总信息
type DownloadBillTotalItem struct {
	TotalCount          int `json:"total_count"`           // 总交易单数
	SettlementTotalFee  int `json:"settlement_total_fee"`  // 应结订单总金额
	SettlementRefundFee int `json:"settlement_refund_fee"` // 退款总金额
	CouponRefundFee     int `json:"coupon_refund_fee"`     // 充值券退款总金额
	ServiceFee          int `json:"service_fee"`           // 手续费总金额
	TotalFee            int `json:"total_fee"`             // 订单总金额
	RefundFee           int `json:"refund_fee"`            // 申请退款总金额
}

// 分解字符串
func dbSplit(line string) (result []string) {
	result = strings.Split(line, ",")
	for i := range result {
		if len(result[i]) == 0 {
			continue
		}
		if result[i][0] == '`' {
			result[i] = result[i][1:]
		}
	}
	return
}

// 解析字符串为整型
func dbConvert(str string, digit int, rate int) (rsp int) {
	if len(str) == 0 {
		return
	}
	parts := strings.Split(str, ".")
	if len(parts) >= 2 && digit > 0 {
		rightPart := parts[1]
		if len(rightPart) == 0 {
			rightPart += "00"
		} else if len(rightPart) == 1 {
			rightPart += "0"
		} else {
			rightPart = rightPart[0:2]
		}
		rightNum, _ := strconv.ParseInt(rightPart, 10, 64)
		rsp += int(rightNum)
	}
	leftNum, _ := strconv.ParseInt(parts[0], 10, 64)
	rsp += int(leftNum) * rate
	return
}

/*
   Copyright 2020 XiaochengTech

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
*/

package wxofficial

import (
	"encoding/json"
	"fmt"

	"gitee.com/xiaochengtech/wechat/util"
)

// 查询自定义菜单接口
// https://developers.weixin.qq.com/doc/offiaccount/Custom_Menus/Querying_Custom_Menus.html
func CustomMenuQuery(accessToken string) (rsp CustomMenuQueryResponse, err error) {
	// 构造请求
	url := fmt.Sprintf("https://api.weixin.qq.com/cgi-bin/get_current_selfmenu_info?access_token=%s", accessToken)
	// 发送数据
	rspData, err := util.HttpGet(url)
	if err != nil {
		return
	}
	if err = json.Unmarshal(rspData, &rsp); err != nil {
		return
	}
	return
}

type CustomMenuQueryResponse struct {
	IsMenuOpen   int `json:"is_menu_open"` // 菜单是否开启，0表示未开启，1表示开启
	SelfMenuInfo struct {
		Button []CustomMenuItem `json:"button"` // 菜单按钮
	} `json:"selfmenu_info"` // 菜单信息
}
